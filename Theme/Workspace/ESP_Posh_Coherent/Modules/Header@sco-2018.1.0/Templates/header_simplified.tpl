<nav class="header-simplified-header">
	<!-- <div class="header-simplified-header-logo" data-view="Header.Logo"></div> -->
	<div class="header-simplified-header-logo" data-view="Header.Logo">
		
		<div id="site-logo" class="content-banner"></div><a class="header-logo" href="/" data-touchpoint="home" data-hashtag="#" title="Transaction Accelerator"><img class="header-logo-image" src="{{getThemeAssetsPath extraFooterView.logoUrl 'img/IS_websitelogo_White.png'}}" alt=""></a>

	</div>	
	{{!--
	{{getThemeAssetsPathWithDefault extraFooterView.logoUrl 'img/489px-Two_Sigma_logo.png'}}
	--}}
	<div id="banner-header-top" class="content-banner banner-header-top" data-cms-area="simplified_header_banner" data-cms-area-filters="global"></div>
</nav>



{{!----
The context variables for this template are not currently documented. Use the {{log this}} helper to view the context variables in the Console of your browser's developer tools.

----}}
