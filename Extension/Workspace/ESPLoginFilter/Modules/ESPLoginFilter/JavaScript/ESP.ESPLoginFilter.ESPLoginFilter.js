
define(
	'ESP.ESPLoginFilter.ESPLoginFilter'
,   [
		'ESP.ESPLoginFilter.ESPLoginFilter.View'
	,	'SC.Configuration'
	,	'jQuery'
	,	'ErrorManagement'
	,	'GlobalViews.Message.View'
	]
,   function (
		ESPLoginFilterView
	,	Configuration
	,	jQuery
	,	ErrorManagement
	,	GlobalViewsMessageView
	)
{
	'use strict';

	return  {
		mountToApp: function mountToApp (container)
		{

			var LoginRegisterPage = container.getComponent('LoginRegisterPage');
			var emailDomains = Configuration.get('EmailDomain.config');
			var companyName = Configuration.get('SiteDomain.company');
			
			if (LoginRegisterPage)
			{

				LoginRegisterPage.on('beforeLogin', function (data)
				{	
						
					var email = data.email; 
					var domain = email.substring(email.lastIndexOf("@") +1);
			
					container.getLayout().$('[data-type=alert-placeholder]').empty();

					if(email){

						if(!emailDomains.includes(domain)){
				
						var global_view_message = new GlobalViewsMessageView({
							message: 'Please use your '+ companyName +' email address to log in.',
							type: 'error',
							closable: true
						});
						
						container.getLayout().$('[data-type=alert-placeholder]').prepend(
							global_view_message.render().$el
						);

						return jQuery.Deferred().reject();

						}
					}
				})


				LoginRegisterPage.on('beforeRegister', function (formFields)
				{	
					container.getLayout().$('[data-type=alert-placeholder]').empty();
					
					var email = formFields.email; 
					var domain = email.substring(email.lastIndexOf("@") +1);
					
					if(email){

						if(!emailDomains.includes(domain)){
					
						var global_view_message = new GlobalViewsMessageView({
							message: 'Your email is not allowed to register.',
							type: 'error',
							closable: true
						});
						
						container.getLayout().$('[data-type=alert-placeholder]').prepend(
							global_view_message.render().$el
						);

							return jQuery.Deferred().reject();

						}
					}
				})
			}
		}
	};
});
