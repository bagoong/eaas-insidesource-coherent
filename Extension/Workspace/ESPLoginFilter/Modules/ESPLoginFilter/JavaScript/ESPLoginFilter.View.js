// @module ESP.ESPLoginFilter.ESPLoginFilter
define('ESP.ESPLoginFilter.ESPLoginFilter.View'
,	[
	'esp_esploginfilter_esploginfilter.tpl'
	
	,	'ESP.ESPLoginFilter.ESPLoginFilter.SS2Model'
	
	,	'Backbone'
    ]
, function (
	esp_esploginfilter_esploginfilter_tpl
	
	,	ESPLoginFilterSS2Model
	
	,	Backbone
)
{
    'use strict';

	// @class ESP.ESPLoginFilter.ESPLoginFilter.View @extends Backbone.View
	return Backbone.View.extend({

		template: esp_esploginfilter_esploginfilter_tpl

	,	initialize: function (options) {

			/*  Uncomment to test backend communication with an example service
				(you'll need to deploy and activate the extension first)
			*/

			// this.model = new ESPLoginFilterModel();
			// var self = this;
         	// this.model.fetch().done(function(result) {
			// 	self.message = result.message;
			// 	self.render();
      		// });
		}

	,	events: {
		}

	,	bindings: {
		}

	, 	childViews: {

		}

		//@method getContext @return ESP.ESPLoginFilter.ESPLoginFilter.View.Context
	,	getContext: function getContext()
		{
			//@class ESP.ESPLoginFilter.ESPLoginFilter.View.Context
			this.message = this.message || 'Hello World!!'
			return {
				message: this.message
			};
		}
	});
});
