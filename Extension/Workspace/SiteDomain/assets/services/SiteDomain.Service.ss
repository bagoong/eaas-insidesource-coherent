
function service(request, response)
{
	'use strict';
	try 
	{
		require('ESP.SiteDomain.SiteDomain.ServiceController').handle(request, response);
	} 
	catch(ex)
	{
		console.log('ESP.SiteDomain.SiteDomain.ServiceController ', ex);
		var controller = require('ServiceController');
		controller.response = response;
		controller.request = request;
		controller.sendError(ex);
	}
}