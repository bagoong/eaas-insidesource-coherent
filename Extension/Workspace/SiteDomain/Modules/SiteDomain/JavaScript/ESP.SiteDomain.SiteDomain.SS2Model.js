// Model.js
// -----------------------
// @module Case
define("ESP.SiteDomain.SiteDomain.SS2Model", ["Backbone", "Utils"], function(
    Backbone,
    Utils
) {
    "use strict";

    // @class Case.Fields.Model @extends Backbone.Model
    return Backbone.Model.extend({
        //@property {String} urlRoot
        urlRoot: Utils.getAbsoluteUrl(
            getExtensionAssetsPath(
                "Modules/SiteDomain/SuiteScript2/SiteDomain.Service.ss"
            ),
            true
        )
});
});
