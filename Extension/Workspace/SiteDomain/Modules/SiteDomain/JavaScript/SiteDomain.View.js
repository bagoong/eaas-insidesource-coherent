// @module ESP.SiteDomain.SiteDomain
define('ESP.SiteDomain.SiteDomain.View'
,	[
	'esp_sitedomain_sitedomain.tpl'
	,	'Backbone'
	, 'SC.Configuration'
    ]
, function (
	esp_sitedomain_sitedomain_tpl
	,	Backbone
	,	Configuration
)
{
    'use strict';

	// @class ESP.SiteDomain.SiteDomain.View @extends Backbone.View
	return Backbone.View.extend({

		template: esp_sitedomain_sitedomain_tpl

	,	initialize: function (options) {
			var self = this;
			self.siteDomain = Configuration.get('SiteDomain.config', ''); 
		}

		//@method getContext @return ESP.SiteDomain.SiteDomain.View.Context
	,	getContext: function getContext()
		{
			//@class ESP.SiteDomain.SiteDomain.View.Context
			return {
				siteDomain: this.siteDomain
			};
		}
	});
});
