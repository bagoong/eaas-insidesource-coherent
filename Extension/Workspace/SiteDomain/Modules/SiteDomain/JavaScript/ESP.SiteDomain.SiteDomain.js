
define(
	'ESP.SiteDomain.SiteDomain'
,   [
		'ESP.SiteDomain.SiteDomain.View'
	]
,   function (
		SiteDomainView
	)
{
	'use strict';

	return  {
		mountToApp: function mountToApp (container)
		{
			
			var LoginRegisterPage = container.getComponent('LoginRegisterPage');

			if (LoginRegisterPage)
			{
			  LoginRegisterPage.addChildView('Register.CustomFields', function ()
			  {
				return new SiteDomainView
				({
				  LoginRegisterPage: LoginRegisterPage
				})
			  });
			}
		}
	};
});
