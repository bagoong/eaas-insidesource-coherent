<div class="login-register-register-form-controls-group">
  <label id="login-register-register-custentity_site_domain_entfield" class="login-register-register-form-label">
      <input type="hidden" name="custentity_site_domain_entfield" id="custentity_site_domain_entfield" value="{{siteDomain}}">
  </label>
</div>


<!--
  Available helpers:
  {{ getExtensionAssetsPath "img/image.jpg"}} - reference assets in your extension
  
  {{ getExtensionAssetsPathWithDefault context_var "img/image.jpg"}} - use context_var value i.e. configuration variable. If it does not exist, fallback to an asset from the extension assets folder
  
  {{ getThemeAssetsPath context_var "img/image.jpg"}} - reference assets in the active theme
  
  {{ getThemeAssetsPathWithDefault context_var "img/theme-image.jpg"}} - use context_var value i.e. configuration variable. If it does not exist, fallback to an asset from the theme assets folder
-->