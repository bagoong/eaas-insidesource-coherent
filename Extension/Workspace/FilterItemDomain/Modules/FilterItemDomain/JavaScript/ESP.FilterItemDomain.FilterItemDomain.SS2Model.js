// Model.js
// -----------------------
// @module Case
define("ESP.FilterItemDomain.FilterItemDomain.SS2Model", ["Backbone", "Utils"], function(
    Backbone,
    Utils
) {
    "use strict";

    // @class Case.Fields.Model @extends Backbone.Model
    return Backbone.Model.extend({
        //@property {String} urlRoot
        urlRoot: Utils.getAbsoluteUrl(
            getExtensionAssetsPath(
                "Modules/FilterItemDomain/SuiteScript2/FilterItemDomain.Service.ss"
            ),
            true
        )
});
});
