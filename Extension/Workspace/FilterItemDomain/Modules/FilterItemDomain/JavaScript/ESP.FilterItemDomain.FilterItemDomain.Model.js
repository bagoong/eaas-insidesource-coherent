// Model.js
// -----------------------
// @module Case
define("ESP.FilterItemDomain.FilterItemDomain.Model", ["Backbone", "Utils"], function(
    Backbone,
    Utils
) {
    "use strict";

    // @class Case.Fields.Model @extends Backbone.Model
    return Backbone.Model.extend({

        
        //@property {String} urlRoot
        urlRoot: Utils.getAbsoluteUrl(
            getExtensionAssetsPath(
                "services/FilterItemDomain.Service.ss"
            )
        )
        
});
});
