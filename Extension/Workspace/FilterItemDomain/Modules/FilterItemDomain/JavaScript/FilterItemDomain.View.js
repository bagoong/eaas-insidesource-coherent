// @module ESP.FilterItemDomain.FilterItemDomain
define('ESP.FilterItemDomain.FilterItemDomain.View'
,	[
	'esp_filteritemdomain_filteritemdomain.tpl'
	
	,	'ESP.FilterItemDomain.FilterItemDomain.SS2Model'
	
	,	'Backbone'
    ]
, function (
	esp_filteritemdomain_filteritemdomain_tpl
	
	,	FilterItemDomainSS2Model
	
	,	Backbone
)
{
    'use strict';

	// @class ESP.FilterItemDomain.FilterItemDomain.View @extends Backbone.View
	return Backbone.View.extend({

		template: esp_filteritemdomain_filteritemdomain_tpl

	,	initialize: function (options) {

			/*  Uncomment to test backend communication with an example service
				(you'll need to deploy and activate the extension first)
			*/

			// this.model = new FilterItemDomainModel();
			// var self = this;
         	// this.model.fetch().done(function(result) {
			// 	self.message = result.message;
			// 	self.render();
      		// });
		}

	,	events: {
		}

	,	bindings: {
		}

	, 	childViews: {

		}

		//@method getContext @return ESP.FilterItemDomain.FilterItemDomain.View.Context
	,	getContext: function getContext()
		{
			//@class ESP.FilterItemDomain.FilterItemDomain.View.Context
			this.message = this.message || 'Hello World!!'
			return {
				message: this.message
			};
		}
	});
});
