
define(
	'ESP.FilterItemDomain.FilterItemDomain'
,   [
		'ESP.FilterItemDomain.FilterItemDomain.View'
	,	'Utils'	
	,	'SC.Configuration'
	]
,   function (
		FilterItemDomainView
	,	Utils
	,	Configuration
	)
{
	'use strict';

	return  {
		mountToApp: function mountToApp (container)
		{
			var plp = container.getComponent('PLP')
			var layout = container.getComponent('Layout');
			var siteDomainConfig = Configuration.get('SiteDomain.config', '');

			if(layout)
			{
			//	var urlHome = SC.ENVIRONMENT.siteSettings.touchpoints.home;
				
				layout.addToViewContextDefinition('Facets.ItemCell.View', 'isshowItembyDomain', 'boolean', function(context) {
					var model = _.find(plp.getItemsInfo(), function (item)
					{
						return item.internalid == context.itemId
					});

					var custitem_espsite_domain = model.custitem_espsite_domain;
					// console.log(custitem_espsite_domain);
					// console.log(siteDomainConfig);

					var isshowItembyDomain = (custitem_espsite_domain == siteDomainConfig) ;
	
					// console.log({isshowItembyDomain});
					return isshowItembyDomain;
				});
			}

			
		}
	};
});
